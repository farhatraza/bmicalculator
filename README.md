Created By @Farhat Raza.

BMI (Body Mass Index) Calculator
--------------------------------

Body mass index (BMI) is a person's weight in kilograms divided by the square of height in meters. BMI Calculator Application is an inexpensive and easy screening method for weight category—underweight, healthy weight, overweight, and obesity.

To run BMI Claculator First you need to run in visual studio 2019.

Steps to create BMI Calculator was:-

Use XAML to design the appearance of the application's user interface (UI).
Write code to build the application's behavior.
Create an application definition to manage the application.
Add controls and create the layout to compose the application UI.
Create styles for a consistent appearance throughout an application's UI.
You can run this software with exe file as well without using visual studio 2019.


This Application is maintained under MIT Lisence because MIT license gives users express permission to reuse code for any purpose, sometimes even if code is part of proprietary software. As long as users include the original copy of the MIT license in their distribution, they can make any changes or modifications to the code to suit their own needs.