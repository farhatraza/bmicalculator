﻿using System;
using System.Windows;
using System.Windows.Media;

//  AUTHORS - Farhat Raza, Lennox Gilbert, Minaldeep Cheema, Sachita Patel

namespace BMI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        float height;
        float weight;
        float result;
        bool IsMetric = false;
        const int IMPERIAL_CONST = 703;
        const int FOOT_TO_INCH = 12;
        const int METER_TO_CM = 100;
        const double UNDERWEIGHT_BMI_MAX = 18.5;
        const int NORMAL_BMI_MAX = 25;
        const int OVERWEIGHT_BMI_MAX = 30;
        double roundedBMI;

        public MainWindow()
        {
            InitializeComponent();
            ImperialOption.IsChecked = true;
            GrayOutCategories();
        }

        private void ImperialRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            MetricGrid.Visibility = Visibility.Collapsed;
            ImperialGrid.Visibility = Visibility.Visible;
            IsMetric = false;
        }

        private void MetricOption_Checked(object sender, RoutedEventArgs e)
        {
            ImperialGrid.Visibility = Visibility.Collapsed;
            MetricGrid.Visibility = Visibility.Visible;
            IsMetric = true;
        }

        private void MetricBMICalc()
        {
            height = float.Parse(HeightCentimeterInput.Text) / METER_TO_CM;
            weight = float.Parse(WeightKgInput.Text);
            result = weight / (height * height);
            roundedBMI = Math.Round(result, 2);
            ResultBlock.Text = roundedBMI.ToString();
        }

        private void ImperialBMICalc()
        {
            float feet = !String.IsNullOrEmpty(HeightFeetInput.Text) ? float.Parse(HeightFeetInput.Text) : 0;
            float inches = !String.IsNullOrEmpty(HeightInchInput.Text) ? float.Parse(HeightInchInput.Text) : 0;
            height = feet * FOOT_TO_INCH + inches;
            weight = float.Parse(WeightLbInput.Text);
            result = (IMPERIAL_CONST * weight) / (height * height);
            roundedBMI = Math.Round(result, 2);
            ResultBlock.Text = roundedBMI.ToString();
        }

        private void BMICategoryReset()
        {
            BMIUnderWeightBlock.Background = new SolidColorBrush(Colors.YellowGreen);
            BMINormalWeightBlock.Background = new SolidColorBrush(Colors.Green);
            BMIOverWeightBlock.Background = new SolidColorBrush(Colors.OrangeRed);
            BMIObeseBlock.Background = new SolidColorBrush(Colors.DarkRed);
        }

        private void CheckBMIButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (IsMetric)
                {
                    MetricBMICalc();
                }
                else
                {
                    ImperialBMICalc();
                }

                CollapseRadioGrid();

                if (result < UNDERWEIGHT_BMI_MAX)
                {
                    BMICategoryReset();

                    BMINormalWeightBlock.Background = new SolidColorBrush(Colors.Gray);
                    BMIOverWeightBlock.Background = new SolidColorBrush(Colors.Gray);
                    BMIObeseBlock.Background = new SolidColorBrush(Colors.Gray);

                    UnderWeightImg.Visibility = Visibility.Visible;
                    NormalImg.Visibility = Visibility.Hidden;
                    OverweightImg.Visibility = Visibility.Hidden;
                    ObesityClass1Img.Visibility = Visibility.Hidden;

                    TextLabel.Content = "";
                }
                else if (result < NORMAL_BMI_MAX)
                {
                    BMICategoryReset();

                    BMIUnderWeightBlock.Background = new SolidColorBrush(Colors.Gray);
                    BMIOverWeightBlock.Background = new SolidColorBrush(Colors.Gray);
                    BMIObeseBlock.Background = new SolidColorBrush(Colors.Gray);

                    UnderWeightImg.Visibility = Visibility.Hidden;
                    NormalImg.Visibility = Visibility.Visible;
                    OverweightImg.Visibility = Visibility.Hidden;
                    ObesityClass1Img.Visibility = Visibility.Hidden;

                    TextLabel.Content = "";
                }
                else if (result < OVERWEIGHT_BMI_MAX)
                {
                    BMICategoryReset();

                    BMIUnderWeightBlock.Background = new SolidColorBrush(Colors.Gray);
                    BMINormalWeightBlock.Background = new SolidColorBrush(Colors.Gray);
                    BMIObeseBlock.Background = new SolidColorBrush(Colors.Gray);

                    UnderWeightImg.Visibility = Visibility.Hidden;
                    NormalImg.Visibility = Visibility.Hidden;
                    OverweightImg.Visibility = Visibility.Visible;
                    ObesityClass1Img.Visibility = Visibility.Hidden;

                    TextLabel.Content = "";
                }
                else if (result >= OVERWEIGHT_BMI_MAX)
                {
                    BMICategoryReset();

                    BMIUnderWeightBlock.Background = new SolidColorBrush(Colors.Gray);
                    BMINormalWeightBlock.Background = new SolidColorBrush(Colors.Gray);
                    BMIOverWeightBlock.Background = new SolidColorBrush(Colors.Gray);

                    UnderWeightImg.Visibility = Visibility.Hidden;
                    NormalImg.Visibility = Visibility.Hidden;
                    OverweightImg.Visibility = Visibility.Hidden;
                    ObesityClass1Img.Visibility = Visibility.Visible;

                    TextLabel.Content = "";
                }
                else
                {
                    BMICategoryReset();
                    ShowRadioGrid();

                    BMIUnderWeightBlock.Background = new SolidColorBrush(Colors.Gray);
                    BMINormalWeightBlock.Background = new SolidColorBrush(Colors.Gray);
                    BMIOverWeightBlock.Background = new SolidColorBrush(Colors.Gray);
                    BMIObeseBlock.Background = new SolidColorBrush(Colors.Gray);

                    UnderWeightImg.Visibility = Visibility.Hidden;
                    NormalImg.Visibility = Visibility.Hidden;
                    OverweightImg.Visibility = Visibility.Hidden;
                    ObesityClass1Img.Visibility = Visibility.Hidden;

                    TextLabel.Foreground = new SolidColorBrush(Colors.Red);
                    TextLabel.Content = "Please enter a valid value in the fields";
                }
            }
            catch
            {
                TextLabel.Foreground = new SolidColorBrush(Colors.Red);
                TextLabel.Content = "Please enter a valid value in the fields";
            }
        }

        private void CollapseRadioGrid()
        {
            grid_radioButton.Visibility = Visibility.Collapsed;
            grid_image.Visibility = Visibility.Visible;
        }

        private void ShowRadioGrid()
        {
            grid_image.Visibility = Visibility.Collapsed;
            grid_radioButton.Visibility = Visibility.Visible;
        }

        private void About_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Body mass index (BMI) is a measure of body fat based on height and weight that applies to adult men and women." +
                "\n\nEnter your weight and height using the imperial or metric measures and see what category you fall under.");
        }

        private void Reset_Calc_Click(object sender, RoutedEventArgs e)
        {
            BMICategoryReset();
            GrayOutCategories();
            HideVisualMan();
            ShowRadioGrid();

            ResultBlock.Text = "";
            HeightCentimeterInput.Text = "";
            HeightFeetInput.Text = "";
            HeightInchInput.Text = "";
            WeightKgInput.Text = "";
            WeightLbInput.Text = "";
            TextLabel.Content = "";

        }

        private void GrayOutCategories()
        {
            BMIUnderWeightBlock.Background = new SolidColorBrush(Colors.Gray);
            BMINormalWeightBlock.Background = new SolidColorBrush(Colors.Gray);
            BMIOverWeightBlock.Background = new SolidColorBrush(Colors.Gray);
            BMIObeseBlock.Background = new SolidColorBrush(Colors.Gray);
        }

        private void HideVisualMan()
        {
            UnderWeightImg.Visibility = Visibility.Hidden;
            NormalImg.Visibility = Visibility.Hidden;
            OverweightImg.Visibility = Visibility.Hidden;
            ObesityClass1Img.Visibility = Visibility.Hidden;
        }
    }
}
